﻿using Grpc.Core;
using Grpc.Net.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ChatClientWPF
{
    class Implementation
    {
        private static ChatService.ChatServiceClient client;
        private static AsyncDuplexStreamingCall<ChatRequest, ChatReply> call;
        private static string name;
        public static string NameClient
        {
            set
            {
                name = value;
            }
            get
            {
                return name;
            }
        }

        public Implementation()
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            client = new ChatService.ChatServiceClient(channel);
            call = client.chat();
        }
        public static async Task TaskAsync(string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                return;
            }
            await call.RequestStream.WriteAsync(new ChatRequest { Name = NameClient, Message = message });
        }
        public static async Task Disconnect()
        {
            MessageBox.Show("Disconnecting...");
            var readTask = Task.Run(async () =>
            {
                await foreach (var response in call.ResponseStream.ReadAllAsync())
                    MessageBox.Show(response.Name + ": " + response.Message);
             
            });
            await call.RequestStream.WriteAsync(new ChatRequest { Status = true, Name = NameClient });
            await call.RequestStream.CompleteAsync();
            await readTask;
        }
    }
}
