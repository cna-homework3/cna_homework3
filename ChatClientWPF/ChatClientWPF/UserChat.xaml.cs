﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ChatClientWPF
{
    /// <summary>
    /// Interaction logic for UserChat.xaml
    /// </summary>
    public partial class UserChat : Window
    {
        public UserChat()
        {
            InitializeComponent();
            Closing += new System.ComponentModel.CancelEventHandler(UserChat_Disconnect);
            GetMessages();
        }

        private async void SendMessage_Click(object sender, RoutedEventArgs e)
        {
            string message = MessageBox.Text.ToString();
            MessageBox.Clear();
            await Implementation.TaskAsync(message);
        }
        public void GetMessages()
        {
            MessagesBlock.Inlines.Add(new Run($"{Implementation.NameClient}: "));
            MessagesBlock.Inlines.Add(new Run("\n"));
            Scroll.ScrollToBottom();
        }
        async void UserChat_Disconnect(object sender, System.ComponentModel.CancelEventArgs e)
        {
            await Implementation.Disconnect();
        }
    }
}
