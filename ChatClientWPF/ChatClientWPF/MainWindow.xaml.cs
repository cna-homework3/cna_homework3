﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ChatClientWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            if (Nickname.Text.ToString() != "")
            {
                Implementation implementation = new Implementation();
                Implementation.NameClient = Nickname.Text.ToString();
                this.Close();
                UserChat userChat = new UserChat();
                userChat.Show();
            }
            else
            {
                MessageBox.Show("Introduce your nickname!");
            }
        }
    }
}
